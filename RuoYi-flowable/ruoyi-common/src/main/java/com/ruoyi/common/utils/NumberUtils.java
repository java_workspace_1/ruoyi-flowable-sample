package com.ruoyi.common.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * @Description 数字转换工具类
 * @Author zhouhouliang
 * @Date 2024/1/19 16:24
 */
public class NumberUtils {

   public static   Long parseLong(String num) {
        if (StringUtils.isBlank(num)) {
            return null;
        }else{
            return Long.parseLong(num);
        }
    }

}
