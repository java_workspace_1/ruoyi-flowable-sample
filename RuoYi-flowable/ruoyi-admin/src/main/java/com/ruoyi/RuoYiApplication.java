package com.ruoyi;

import com.ruoyi.common.utils.spring.SpringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.core.env.Environment;

/**
 * 启动程序
 *
 * @author ruoyi
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@Slf4j
public class RuoYiApplication {


    public static void main(String[] args) {
        SpringApplication.run(RuoYiApplication.class, args);

        Environment environment = SpringUtils.getBean(Environment.class);
        String url = "http://127.0.0.1:" + environment.getProperty("server.port") + environment.getProperty("server.servlet.context-path");
        log.info(
                "(♥◠‿◠)ﾉﾞ  若依启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                        " {}\n" +
                        " .-------.       ____     __        \n" +
                        " |  _ _   \\      \\   \\   /  /    \n" +
                        " | ( ' )  |       \\  _. /  '       \n" +
                        " |(_ o _) /        _( )_ .'         \n" +
                        " | (_,_).' __  ___(_ o _)'          \n" +
                        " |  |\\ \\  |  ||   |(_,_)'         \n" +
                        " |  | \\ `'   /|   `-'  /           \n" +
                        " |  |  \\    /  \\      /           \n" +
                        " ''-'   `'-'    `-..-'              ", url);
    }
}
