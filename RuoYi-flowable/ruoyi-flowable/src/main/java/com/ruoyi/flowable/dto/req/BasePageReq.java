package com.ruoyi.flowable.dto.req;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "查询基类")
public class BasePageReq {

    @ApiModelProperty(value = "页码", required = true)
    @NotNull(message = "pageNum is null")
    private Integer pageNum = 1;

    @NotNull(message = "pageNum is null")
    @ApiModelProperty(value = "页大小", required = true)
    private Integer pageSize = 10;

    @ApiModelProperty(value = "排序字段", required = false, notes = "默认按照工单号倒序")
    private List<OrderItem> orderItems;

    public List<String> parseOrderItemStatements() {
        if (orderItems != null && orderItems.size() > 0) {
            return orderItems.stream().map(orderItem -> "`" + orderItem.getKey() + "` " + orderItem.getType()).collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class OrderItem {
        @ApiModelProperty(value = "排序字段(驼峰命名)", required = true)
        @NotBlank(message = "key is null")
        private String key;

        @ApiModelProperty(value = "排序方式", required = true, allowableValues = "asc,desc")
        @NotBlank(message = "type is null")
        private String type;
    }

}
