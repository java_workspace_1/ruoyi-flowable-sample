package com.ruoyi.flowable.dto.req;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @Description 流程定义查询请求
 * @Author zhouhouliang
 * @Date 2024/1/15 17:07
 */
@Data
@Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DefinitionReq extends BasePageReq {
    private String key;
    private String name;
    private Boolean isNewest;
}
